﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
   [SerializeField] private float _speed = 8f;
   [SerializeField] private float _jumpHeight = 8;
   [SerializeField] private float _maxVelocity = 3f;
   private Rigidbody2D _rbPlayer;
   
   private void Awake()
   {
      _rbPlayer = GetComponent<Rigidbody2D>();
   }

   private void FixedUpdate()
   {
      PlayerMovement();
   }

   private void PlayerMovement()
   {
      float forceX = 0f;
      float velocity = Math.Abs(_rbPlayer.velocity.x);
      float direct = Input.GetAxisRaw("Horizontal"); //-1 0 1
      if (direct > 0)
      {
         if (velocity < _maxVelocity) forceX = _speed;
      }
      else if (direct < 0)
      {
         if (velocity < _maxVelocity) forceX = -_speed;
      }
      _rbPlayer.AddForce(new Vector2(forceX,0));

      if (Input.GetKeyDown(KeyCode.Space))
      {
         Vector3 playerTranform = transform.localPosition;
         playerTranform.y = _jumpHeight;
         transform.DOLocalJump(playerTranform, 3, 1,0.3f);
      }
   }
}
